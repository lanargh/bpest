clear all;
close all;
clc;
%% load data
fprintf('Loading Data .. . ..\n');
load new_dataset_use_7_units;
%remove = find(parameter_features(:,10)>180|parameter_features(:,11)>130|parameter_features(:,10)<80|parameter_features(:,11)<60);
%parameter_features(remove,:) = [];
fprintf('...done\n');

features = parameter_features(:, 3:9);

%% data partition
%[train,val,test] = holdout(parameter_features(1:1000,:),80);
[train,val,test] = holdout(parameter_features,80);
% train = parameter_features(1:80,:);
% test = parameter_features(1:200,:);

%% Features (7 features)
X_Train = train(:,3:9);
X_Train = num2cell(X_Train,2);

for i = 1:numel(X_Train)
    X_Train{i,1} = X_Train{i,1}';
end

X_Val = val(:,3:9);
X_Val = num2cell(X_Val,2);

for i = 1:numel(X_Val)
    X_Val{i,1} = X_Val{i,1}';
end

X_Test = test(:,3:9);
X_Test = num2cell(X_Test,2);

for i = 1:numel(X_Test)
    X_Test{i,1} = X_Test{i,1}';
end

%% normalize the data using zero-mean and unit-variance.
mu = mean([X_Train{:}],2);
sig = std([X_Train{:}],0,2);

for i = 1:numel(X_Train)
    X_Train{i} = (X_Train{i} - mu) ./ sig;
end

mu = mean([X_Val{:}],2);
sig = std([X_Val{:}],0,2);

for i = 1:numel(X_Val)
    X_Val{i} = (X_Val{i} - mu) ./ sig;
end

mu = mean([X_Test{:}],2);
sig = std([X_Test{:}],0,2);

for i = 1:numel(X_Test)
    X_Test{i} = (X_Test{i} - mu) ./ sig;
end

%% LSTM Model
numResponses = 1;
numHiddenUnits1 = 256;
numHiddenUnits2 = 256;

featureDimension = size(X_Train{1},1);

layers = [ ...
    sequenceInputLayer(featureDimension, 'Name','input')
    bilstmLayer(numHiddenUnits1,'OutputMode','sequence','Name', 'bilstm')
    dropoutLayer(0.2, 'Name', 'do1')
    reluLayer('Name','relu1')

    lstmLayer(numHiddenUnits2,'OutputMode','sequence','Name', 'lstm1')
    dropoutLayer(0.2, 'Name', 'do2')
    reluLayer('Name','relu2')

    lstmLayer(numHiddenUnits2,'OutputMode','sequence','Name', 'lstm2')
    dropoutLayer(0.2, 'Name', 'do3')
    reluLayer('Name','relu3')

    additionLayer(2,'Name','add2')
    
    lstmLayer(numHiddenUnits2,'OutputMode','sequence','Name', 'lstm3')
    dropoutLayer(0.2, 'Name', 'do4')
    reluLayer('Name','relu4')

    additionLayer(2,'Name','add3')
    additionLayer(2,'Name','add5')
    
    lstmLayer(numHiddenUnits2,'OutputMode','sequence','Name', 'lstm4')
    dropoutLayer(0.2, 'Name', 'do5')
    reluLayer('Name','relu5')
     
     additionLayer(2,'Name','add4')
     additionLayer(2,'Name','add6')
%      additionLayer(2,'Name','add8')
      
     lstmLayer(numHiddenUnits2, 'Name', 'lstm5')
     dropoutLayer(0.2, 'Name', 'do6')
     reluLayer('Name','relu6')
      
     additionLayer(2,'Name','add1')
    additionLayer(2,'Name','add7')
    additionLayer(2,'Name','add9')
%    additionLayer(2,'Name','add10')
% 
%     lstmLayer(numHiddenUnits2, 'Name', 'lstm6')
%     dropoutLayer(0.2, 'Name', 'do7')
%     reluLayer('Name','relu7')

    fullyConnectedLayer(numResponses, 'Name', 'fc2')
    regressionLayer('Name','routput')];

lgraph = layerGraph(layers);
figure
plot(lgraph)

lgraph = connectLayers(lgraph,'relu2','add2/in2');
lgraph = connectLayers(lgraph,'relu2','add3/in2');
lgraph = connectLayers(lgraph,'relu2','add4/in2');
lgraph = connectLayers(lgraph,'relu2','add1/in2');
lgraph = connectLayers(lgraph,'relu3','add5/in2');
lgraph = connectLayers(lgraph,'relu3','add6/in2');
lgraph = connectLayers(lgraph,'relu3','add7/in2');
%lgraph = connectLayers(lgraph,'relu4','add8/in2');
lgraph = connectLayers(lgraph,'relu4','add9/in2');
%lgraph = connectLayers(lgraph,'relu5','add10/in2');

figure
plot(lgraph);

%load './checkpoint_path_19/model_4_sbp.mat';

%% normalize the SBP become 0-1
y_Train = train(:,10);
%maxyTrain = max(y_Train);
maxyTrain = 180;
for i = 1:numel(y_Train)
    y_Train(i) = (y_Train(i)) ./ maxyTrain;
end
y_Train = num2cell(y_Train,2);

y_Val = val(:,10);
%maxyVal = max(y_Train);
maxyVal = 180;
for i = 1:numel(y_Val)
    y_Val(i) = (y_Val(i)) ./ maxyVal;
end
y_Val = num2cell(y_Val,2);

y_Test = test(:,10);
%maxyTest = max(y_Test);
maxyTest = 180;
for i = 1:numel(y_Test)
    y_Test(i) = (y_Test(i)) ./ maxyTest;
end
y_Test = num2cell(y_Test,2);

%% initializations SBP
maxEpochs_SBP = 50;

options = trainingOptions('adam', ...
    'MaxEpochs',maxEpochs_SBP, ...
    'GradientThreshold',1, ...
    'InitialLearnRate',0.003, ...
    'LearnRateSchedule','piecewise', ...
    'Shuffle','never', ...
    'LearnRateDropPeriod',5000, ...
    'LearnRateDropFactor',0.2, ...
    'ValidationData',{X_Val,y_Val}, ...
    'ValidationFrequency',1000, ...
    'Verbose',0, ...
    'Plots','training-progress');

%% start traning SBP
fprintf('Starting training LSTM Model for SBP...\n');
net = trainNetwork(X_Train,y_Train,lgraph,options);
fprintf('Done ...\n');
fprintf('Starting testing LSTM Model for SBP ...\n');
y_model = predict(net,X_Test);
fprintf('Done ...\n');

%% Back to the real
for i = 1:numel(y_model)
    y_model{i} = (y_model{i}) * maxyTest;
end
for i = 1:numel(y_Test)
    y_Test{i} = (y_Test{i}) * maxyTest;
end
YPred_SBP = round(cell2mat(y_model),2);
YTest_SBP = round(cell2mat(y_Test),2);

rmse1 = sqrt(mean((YPred_SBP - YTest_SBP).^2))

%figure; plot(YPred_SBP);hold on; plot(YTest_SBP);
%figure; plot(YPred_SBP - YTest_SBP);
%histogram(YPred_SBP - YTest_SBP);

title("RMSE = " + rmse1)
ylabel("Frequency")
xlabel("Error")

figure
subplot(2,1,1)
plot(YTest_SBP)
hold on
plot(YPred_SBP,'.-')
hold off
legend(["Observed" "Predicted"])
ylabel("SBP (mmHg)")
title("Estimation of SBP Value")
subplot(2,1,2)
stem(YPred_SBP - YTest_SBP)
xlabel("SBP Values")
ylabel("Error")
title("RMSE = " + rmse1)

%% result of SBP
MAE_LSTM_SBP = sum(abs(YPred_SBP-YTest_SBP))/length(YTest_SBP)
STD_LSTM_SBP = std(YPred_SBP)
rmse_sbp = sqrt(mean((YPred_SBP - YTest_SBP).^2))

file_name = sprintf('./save_param/net_sbp_5.mat');
save(file_name,'net','-v6') 

%% normalize the DBP become 0-1
y_Train = train(:,11);
%maxyTrain = max(y_Train);
maxyTrain = 130;
for i = 1:numel(y_Train)
    y_Train(i) = (y_Train(i)) ./ maxyTrain;
end
y_Train = num2cell(y_Train,2);

y_Val = val(:,10);
%maxyVal = max(y_Train);
maxyVal = 180;
for i = 1:numel(y_Val)
    y_Val(i) = (y_Val(i)) ./ maxyVal;
end
y_Val = num2cell(y_Val,2);

y_Test = test(:,11);
%maxyTest = max(y_Test);
maxyTest = 130;
for i = 1:numel(y_Test)
    y_Test(i) = (y_Test(i)) ./ maxyTest;
end
y_Test = num2cell(y_Test,2);

%% initialization DBP
maxEpochs_DBP = 50;

%checkpointPath = './checkpoint_path_1';

options = trainingOptions('adam', ...
    'MaxEpochs',maxEpochs_DBP, ...
    'GradientThreshold',1, ...
    'InitialLearnRate',0.003, ...
    'LearnRateSchedule','piecewise', ...
    'Shuffle','never', ...
    'LearnRateDropPeriod',265, ...
    'LearnRateDropFactor',0.2, ...
    'ValidationData',{X_Val,y_Val}, ...
    'ValidationFrequency',5000, ...
    'Verbose',0, ...
    'Plots','training-progress');

%load './checkpoint_path_19/model_4_dbp.mat'

%% start training DBP
fprintf('\nStarting Training LSTM Model for DBP...\n');
net = trainNetwork(X_Train,y_Train,lgraph,options);
fprintf('Done ...\n');
fprintf('Starting Testing LSTM Model for DBP...\n');
y_model = predict(net,X_Test);
fprintf('Done ...\n');

for i = 1:numel(y_model)
    y_model{i} = (y_model{i}) * maxyTest;
end
for i = 1:numel(y_Test)
    y_Test{i} = (y_Test{i}) * maxyTest;
end

YPred_DBP = round(cell2mat(y_model),2);
YTest_DBP = round(cell2mat(y_Test),2);

%figure
rmse2 = sqrt(mean((YPred_DBP - YTest_DBP).^2))

%figure; plot(YPred_DBP);hold on; plot(YTest_DBP);
%figure; plot(YPred_DBP - YTest_DBP);
%histogram(YPred_DBP - YTest_DBP);

title("RMSE = " + rmse2)
ylabel("Frequency")
xlabel("Error")

MAE_LSTM_DBP = sum(abs(YPred_DBP-YTest_DBP))/length(YTest_DBP)
STD_LSTM_DBP = std(YPred_DBP)

rmse_dbp = sqrt(mean((YPred_DBP - YTest_DBP).^2))

RESULTS = [maxEpochs_SBP maxEpochs_DBP numHiddenUnits1 numHiddenUnits2 MAE_LSTM_SBP MAE_LSTM_DBP STD_LSTM_SBP STD_LSTM_DBP rmse_sbp rmse_dbp];
file_name = sprintf('./save_param/result_5.mat');
save(file_name,'RESULTS','-v6')   
file_name = sprintf('./save_param/net_dbp_5.mat');
save(file_name,'net','-v6') 