function varargout = Demo_BPEst(varargin)
% DEMO_BPEST MATLAB code for Demo_BPEst.fig
%      DEMO_BPEST, by itself, creates a new DEMO_BPEST or raises the existing
%      singleton*.
%
%      H = DEMO_BPEST returns the handle to a new DEMO_BPEST or the handle to
%      the existing singleton*.
%
%      DEMO_BPEST('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in DEMO_BPEST.M with the given input arguments.
%
%      DEMO_BPEST('Property','Value',...) creates a new DEMO_BPEST or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Demo_BPEst_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Demo_BPEst_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Demo_BPEst

% Last Modified by GUIDE v2.5 06-May-2019 16:08:07

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Demo_BPEst_OpeningFcn, ...
                   'gui_OutputFcn',  @Demo_BPEst_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Demo_BPEst is made visible.
function Demo_BPEst_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Demo_BPEst (see VARARGIN)

% Choose default command line output for Demo_BPEst
handles.output = hObject;
logo=imread('logo5_small.jpg');
axes(handles.axes2);
imshow(logo);
cla(handles.axes1)
cla(handles.axes3)
xlabel(handles.axes1,'Seconds');
ylabel(handles.axes1,'Amplitude');

xlabel(handles.axes3,'Seconds');
ylabel(handles.axes3,'Amplitude');

set(handles.text5, 'String', 0);
set(handles.text6, 'String', 0);
set(handles.text13, 'String', 0);
set(handles.text14, 'String', 0);
%set(handles.text8, 'String', 0);
set(handles.text9, 'String', 0);

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Demo_BPEst wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Demo_BPEst_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --------------------------------------------------------------------
function uipushtool1_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to uipushtool1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[fname pname] = uigetfile('*.mat','Please select a MAT file with ECG and PPG data');
load(sprintf('%s%s',pname,fname));
id = split(fname,"_");
name = split(id(2),".");
id_name = name(1);
handles.name = id_name{1};
set(handles.text9, 'String', handles.name);
handles.signal = part_3_cells;

plotSignal(handles);

% Update handles structure
guidata(hObject, handles);

function plotSignal(handles)
% This function is used to plot the animated ECG signal in axes1
% input:  handles    structure with handles and user data (see GUIDATA)
fs=125;
%fs=256;
id_features = handles.name;
ecg=handles.signal(3,:);
ppg=handles.signal(1,:);
abp=handles.signal(2,:);

fresult=fft(ecg);
fresult(1 : round(length(fresult)*5/fs))=0;
fresult(end - round(length(fresult)*5/fs) : end)=0;
ecg=real(ifft(fresult));
    
fresult=fft(ppg);
fresult(1 : round(length(fresult)*1/fs))=0;
fresult(end - round(length(fresult)*1/fs) : end)=0;
ppg=real(ifft(fresult));
    
ecg = (ecg - min(ecg));
ecg = ecg / max(ecg);
ppg = (ppg - min(ppg));
ppg = ppg / max(ppg);

ppg = ppg';
ppg = ppg * 100;
ecg = ecg';
ecg = ecg * 100;

[footIndex1, systolicIndex1, notchIndex1, dicroticIndex1, time, bpwaveform] = ...
BP_annotate(ppg, fs, 1, 'mmHg', 1);
[pks,locs] = findpeaks(ecg,'MinPeakDistance',50, 'MinPeakHeight',90);
locs_ppg = systolicIndex1';
locs = locs/fs;
len_ecg = length(locs);
len_ppg = length(locs_ppg);
len_ppg_ppg = length(ecg)/fs;

file_name = sprintf('parameter_features_new_05_07_Part_3/Subject_new_%s.mat',id_features);
    
load (file_name)
%% load data testing
start_point = parameter_features(:,1);
end_point = parameter_features(:,2);
X_Test = parameter_features(:,3:9);
X_Test = num2cell(X_Test,2);
for j = 1:numel(X_Test)
    X_Test{j,1} = X_Test{j,1}';
end

%% load the model of SBP
load net_checkpoint__200__2019_05_14__22_13_58.mat

%% normalize the data using zero-mean and unit-variance.
mu = mean([X_Test{:}],2);
sig = std([X_Test{:}],0,2);

for k = 1:numel(X_Test)
    X_Test{k} = (X_Test{k} - mu) ./ sig;
end

%% nomalize the features
y_test_sbp = parameter_features(:,10);
maxyTest = max(y_test_sbp);
%maxyTest = 180;
for l = 1:numel(y_test_sbp)
    y_test_sbp(l) = (y_test_sbp(l)) ./ maxyTest;
end

y_test_sbp = num2cell(y_test_sbp,2);

y_model_sbp = predict(net,X_Test,'MiniBatchSize',1);
%% Back to the real 
for m = 1:numel(y_model_sbp)
    y_model_sbp{m} = (y_model_sbp{m}) * maxyTest;
end
for n = 1:numel(y_test_sbp)
    y_test_sbp{n} = (y_test_sbp{n}) * maxyTest;
end

for o = 1:numel(y_test_sbp)
    YTestLast_sbp(o) = y_test_sbp{o}(end);
    YPredLast_sbp(o) = y_model_sbp{o}(end);
end
ave_sbp = round(sum(YPredLast_sbp)/length(YPredLast_sbp));
ave_sbp = num2str(ave_sbp);
%% result of SBP
RMSE_LSTM_SBP = sqrt(mean((YPredLast_sbp - YTestLast_sbp).^2));
MAE_LSTM_SBP = sum(abs(YPredLast_sbp-YTestLast_sbp))/length(YTestLast_sbp);
STD_LSTM_SBP = std(YPredLast_sbp-YTestLast_sbp);

%% load model DBP
load net_checkpoint__200__2019_05_14__22_15_43.mat

%% normalize the dbp features
y_test_dbp = parameter_features(:,11);
maxyTest = max(y_test_dbp);
%maxyTest = 130;
for ia = 1:numel(y_test_dbp)
    y_test_dbp(ia) = (y_test_dbp(ia)) ./ maxyTest;
end

y_test_dbp = num2cell(y_test_dbp,2);

y_model_dbp = predict(net,X_Test,'MiniBatchSize',1);

%% back to real
for ib = 1:numel(y_model_dbp)
    y_model_dbp{ib} = (y_model_dbp{ib}) * maxyTest;
end
for ic = 1:numel(y_test_dbp)
    y_test_dbp{ic} = (y_test_dbp{ic}) * maxyTest;
end

for id = 1:numel(y_test_dbp)
    YTestLast_dbp(id) = y_test_dbp{id}(end);
    YPredLast_dbp(id) = y_model_dbp{id}(end);
end
ave_dbp = round(sum(YPredLast_dbp)/length(YPredLast_dbp));
ave_dbp = num2str(ave_dbp);
%% result of DBP
RMSE_LSTM_DBP = sqrt(mean((YPredLast_dbp - YTestLast_dbp).^2));
MAE_LSTM_DBP = sum(abs(YPredLast_dbp-YTestLast_dbp))/length(YTestLast_dbp);
STD_LSTM_DBP = std(YPredLast_dbp-YTestLast_dbp);

fplot = 0.2;
length_signal = length(ecg);
length_shown=2;
length_window=length_shown*fs;
N=(length_signal-length_window)/round(fs*fplot)
i = 1
for iay = 0:N
    % index for segment to plot
    idx=iay*round(fs*fplot)+[1:length_window];
    % corresponding second
    x=idx/fs;
    axes(handles.axes1);
    plot(x,ecg(idx),'-b'),xline(x(20),'--r','LineWidth',1),xline(x(90),'--r','LineWidth',1);
    axes(handles.axes3);
    plot(x,ppg(idx),'-r'),xline(x(20),'--b','LineWidth',1),xline(x(90),'--b','LineWidth',1);
    if mod(iay,5) == 0 
        set(handles.text5, 'String', YPredLast_sbp(i));
        set(handles.text6, 'String', YPredLast_dbp(i));
        set(handles.text13, 'String', YTestLast_sbp(i));
        set(handles.text14, 'String', YTestLast_dbp(i));
        i = i + 1;
        
        if i == length(YPredLast_sbp)
            set(handles.text17, 'String', RMSE_LSTM_SBP);
            set(handles.text19, 'String', MAE_LSTM_SBP);
            set(handles.text18, 'String', RMSE_LSTM_DBP);
            set(handles.text20, 'String', MAE_LSTM_DBP);
        end
    end

    pause(fplot);
end

% --- Executes during object creation, after setting all properties.
function axes2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to axes2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate axes2
