clc
clear all
close all

%% Load Data and Initialize Variables
fprintf('Loading parameter_features and initializing variables\n');
t = cputime;
parameter_features = [];

myFolder = sprintf('./parameter_features_with_7_units_Part_1/');
filePattern = fullfile(myFolder, '*.mat');
matFiles = dir(filePattern);
for k = 1:length(matFiles)
  baseFileName = matFiles(k).name;
  fullFileName = fullfile(myFolder, baseFileName);
  fprintf(1, 'Now reading %s\n', fullFileName);
  matData(k) = load(fullFileName);
end

%% Concatenate parameter_features from part_1
 for k = 1:length(matFiles)
     parameter_features = [parameter_features; matData(k).parameter_features];
 end

%% Load Data and Initialize Variables
myFolder = sprintf('./parameter_features_with_7_units_Part_2/');
filePattern = fullfile(myFolder, '*.mat');
matFiles = dir(filePattern);
for k = 1:length(matFiles)
  baseFileName = matFiles(k).name;
  fullFileName = fullfile(myFolder, baseFileName);
  fprintf(1, 'Now reading %s\n', fullFileName);
  matData(k) = load(fullFileName);
end

%% Concatenate parameter_features from part_2
 for k = 1:length(matFiles)
     parameter_features = [parameter_features; matData(k).parameter_features];
 end

%% Load Data and Initialize Variables
myFolder = sprintf('./parameter_features_with_7_units_Part_3/');
filePattern = fullfile(myFolder, '*.mat');
matFiles = dir(filePattern);
for k = 1:length(matFiles)
  baseFileName = matFiles(k).name;
  fullFileName = fullfile(myFolder, baseFileName);
  fprintf(1, 'Now reading %s\n', fullFileName);
  matData(k) = load(fullFileName);
end

%% Concatenate parameter_features from Part_3
 for k = 1:length(matFiles)
     if isequal(size(parameter_features), size(matData(k).parameter_features))
        parameter_features = [parameter_features; matData(k).parameter_features];
     else
        disp(k);
     end
 end

 %% Load Data and Initialize Variables
myFolder = sprintf('./parameter_features_with_7_units_Part_4/');
filePattern = fullfile(myFolder, '*.mat');
matFiles = dir(filePattern);
for k = 1:length(matFiles)
  baseFileName = matFiles(k).name;
  fullFileName = fullfile(myFolder, baseFileName);
  fprintf(1, 'Now reading %s\n', fullFileName);
  matData(k) = load(fullFileName);
end

%% Concatenate parameter_features from Part_4
 for k = 1:length(matFiles)
     parameter_features = [parameter_features; matData(k).parameter_features];
end

% only take features from normal SBP and DBP
remove = find(parameter_features(:,10)>180|parameter_features(:,11)>130|parameter_features(:,10)<80|parameter_features(:,11)<60);
%remove = find(parameter_features(:,6)>180|parameter_features(:,7)>130|parameter_features(:,6)<80|parameter_features(:,7)<60);
parameter_features(remove,:) = [];
save('new_dataset_use_7_units.mat','parameter_features','-v4') ;