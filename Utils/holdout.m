
function [train_set, val_set, test_set] = holdout (A, p)
    n = size(A,1);
    n_tr = floor((n*(p-10)*0.01));
    n_vl = floor((n*10*0.01));
    n_ts = n-n_tr-n_vl;
    index = randperm(n);
    test_set = zeros(n_ts, size(A,2));
    val_set = zeros(n_vl, size(A,2));
    train_set = zeros(n_tr, size(A,2));
    
    for i = 1:n_ts
        test_set(i,:) = A(index(i),:);
    end
    
    for i = 1:n_vl
        val_set(i,:) = A(index(i+n_ts),:);
    end
    
    for i = 1:n_tr
        train_set(i,:) = A(index(i+n_ts+n_vl),:);
    end
end