function xp = dftinterp(inter, x, n, p)
    c = inter(1); d = inter(2);
    t = c+(d-c)*(0:n-1)/n; % n evenly-spaced time points
    tp = c+(d-c)*(0:p-1)/p; % p evenly-spaced time points
    y = fft(x); % apply DFT
    yp = zeros(p,1); % yp will hold coefficients for ifft
    yp(uint8(1:n/2+1)) = y(uint8(1:n/2+1)); % move n frequencies from n to p
    yp(uint8(p-n/2+2:p)) = y(uint8(n/2+2:n)); % same for upper tier
    xp = (real(ifft(yp))*(p/n)); % invert fft to recover data 
end