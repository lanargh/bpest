clear all;
close all;
clc;

here = mfilename('fullpath');
[path, ~, ~] = fileparts(here);
addpath(genpath(path));

%%
load Part_4
sampling_rate = 125;

for id = 1:length(Part_4)
    close all
    
    % 3 input signals
    ecg = Part_4{1,id}(3:3:end);
    ppg = Part_4{1,id}(1:3:end);
    abp = Part_4{1,id}(2:3:end);
    
    % Features
    f_hr_values = [];
    f_ptt_values = [];
    f_ai_values = [];
    f_uptime_values = [];
    f_syst_time_values = [];
    f_dv_values = [];
    f_sv_values = [];
    f_dptt1_values = [0];
    f_dptt2_values = [0];
    f_duptime1_values = [0];
    f_dsyst_time1_values = [0];
    f_duptime2_values = [0];
    f_dsyst_time2_values = [0];
    
    parameter_features = [];
   
    % Groundtruth signal
    gt_abp_values = [];
    
    the_first_point = [];
    the_second_point = [];

    %% PREPROCESSING
    k = 30;    
    shifted_ecg = circshift(ecg,k);
    shifted_ecg = shifted_ecg * 10000;
    
    %   Transform ecg & ppg into frequency spectrum - Remove the low
    %   frequency components - Inverse to transform back to time domain
    ecg_fftresult = fft(shifted_ecg);
    ecg_fftresult(1 : round(length(ecg_fftresult)*1/sampling_rate)) = 0;
    ecg_fftresult(end - round(length(ecg_fftresult)*1/sampling_rate) : end) = 0;
    filtered_ecg = real(ifft(ecg_fftresult));
    
    ppg_fftresult = fft(ppg);
    ppg_fftresult(1 : round(length(ppg_fftresult) * 1/sampling_rate)) = 0;
    ppg_fftresult(end - round(length(ppg_fftresult) * 1/sampling_rate) : end) = 0;
    filtered_ppg = real(ifft(ppg_fftresult));
    
    %   Filter - first pass
    win_size = floor(sampling_rate * 571 / 1000);
    if rem(win_size, 2) ==0
        win_size = win_size+1;
    end
    detected_ecg_peaks = ecgwindowedfilter(filtered_ecg, win_size);
    
    %   Scale ecg
    r_peaks = detected_ecg_peaks/(max(detected_ecg_peaks)/7);
    
    %   Filter by threshold filter
    for data = 1:1:length(r_peaks)
        if r_peaks(data) < 4
            r_peaks(data) = 0;
        else
            r_peaks(data) = 1;
        end
    end
    
    locs_ecg_peak = find(r_peaks);
    if length(locs_ecg_peak) == 1
        continue
    end
   
    r_distance = locs_ecg_peak(2)-locs_ecg_peak(1);
    if r_distance > 700
        continue
    end
    
    %   Returns minimum distance between two peaks
    for data = 1:1:length(locs_ecg_peak)-1
        if locs_ecg_peak(data+1)-locs_ecg_peak(data) < r_distance 
            r_distance = locs_ecg_peak(data+1)-locs_ecg_peak(data);
        end
    end
    
    %   Runs 2 examples of BP_annotate
    filtered_ppg = filtered_ppg';
    [footIndex, systolicIndex, notchIndex, dicroticIndex ] = ...
    ppgannotation(filtered_ppg, sampling_rate, 1, 'other', 1);
    filtered_ecg = filtered_ecg';
    systolic_peak = unique(systolicIndex)';
    foot = unique(footIndex);
    notch = unique(notchIndex);
    dicrotic_peak = unique(dicroticIndex);
    
    %% NORMALIZATION (0,1)
    filtered_ecg = (filtered_ecg - min(filtered_ecg));
    normalized_ecg = filtered_ecg / max(filtered_ecg);
    filtered_ppg = (filtered_ppg - min(filtered_ppg));
    normalized_ppg = filtered_ppg / max(filtered_ppg);
    
    %% PLOTTING
    % Uncomment to see the plot
%     figure;  plot(normalized_ecg);
%     hold on; plot(locs_ecg_peak,normalized_ecg(locs_ecg_peak),'*');
%     Colors = get(gca, 'ColorOrder');
%     hold on; plot(normalized_ppg);
%     hold on; plot(systolic_peak,normalized_ppg(systolic_peak),'<', 'color', Colors(4,:), 'markerfacecolor', Colors(4,:));
%     hold on; plot(foot,normalized_ppg(foot),'^', 'color', Colors(5,:), 'markerfacecolor', Colors(5,:));
%     hold on; plot(notch,normalized_ppg(notch),'^', 'color', Colors(6,:), 'markerfacecolor', Colors(6,:));
%     hold on; plot(dicrotic_peak,normalized_ppg(dicrotic_peak),'^', 'color', Colors(7,:), 'markerfacecolor', Colors(7,:));
%     legend({'ECG Signal','ECG Peak','PPG Signal','PPG Peak','Foot', 'Notch', 'Dicrotic Peak'},'box','off');
    
    %% LOAD NOISE DATA
    P = './Data_txt_with_units_Part_4';
    F = sprintf('S%i.txt',id);
    S = dir(fullfile(P,F));
    C = [];
    
    if(length(locs_ecg_peak) > length(systolic_peak))
        loccc = length(systolic_peak);
    else
        loccc = length(locs_ecg_peak);
    end
    
    fignum = 1;
    for r_peaks = 1:loccc-2
        % SKIP SIGNAL WITH NOISE
        if r_peaks == loccc
            continue
        else
            ecg_peak1 = locs_ecg_peak(r_peaks);
            ecg_peak2 = locs_ecg_peak(r_peaks+1);
            ppg_peak1 = systolic_peak(r_peaks);
            ppg_peak2 = systolic_peak(r_peaks+1);

            dist_error = abs(ppg_peak2 - ecg_peak1);
            iiiii = systolic_peak >= ecg_peak1 & systolic_peak <= ecg_peak2;
            PPG_peak_find = systolic_peak(iiiii);
            
            if(dist_error > 200)
                ecg_peak1 = locs_ecg_peak(r_peaks+1);
                ecg_peak2 = locs_ecg_peak(r_peaks+2);
            elseif (dist_error < 100 && dist_error > 0)
                ppg_peak1 = systolic_peak(r_peaks+1);
                ppg_peak2 = systolic_peak(r_peaks+2);
            end
            
            part_ecg = normalized_ecg(ecg_peak1:ppg_peak2);
            part_abp = abp(ecg_peak1:ppg_peak2);
            part_ppg = normalized_ppg(ecg_peak1:ppg_peak2);
            
            if isempty(part_ecg)
                continue
            end
            
            part = normalized_ppg(ppg_peak1:ppg_peak2);

            ii = foot >= ecg_peak1 & foot <= ecg_peak2;
            iii = notch >= ecg_peak1 & notch <= ppg_peak2;
            iiii = foot >= ecg_peak1 & foot <= ppg_peak2;
            PPG_min_positions = foot(ii) + 1;
            PPG_min_positions_after = foot(iiii) + 1;
            
            if (length(PPG_min_positions_after) > 1)
                PPG_min_positions_after = max(PPG_min_positions_after);
            end
            if (length(PPG_min_positions) > 1)
                PPG_min_positions = max(PPG_min_positions);
            end
            dict_notch = notch(iii);
            
            if(length(dict_notch) > 1)
                dict_notch = max(dict_notch);
            end
            part_dv = normalized_ppg(PPG_min_positions:dict_notch);
            part_sv = normalized_ppg(dict_notch:PPG_min_positions_after);
            slope = maxslope(normalized_ppg,ecg_peak1,ecg_peak2);
            inf_post = inflectionpoint(normalized_ppg,ppg_peak1,ecg_peak2);
            
            if isempty(S) == 0
                % File exists.
                N = S.name;
                filename = fullfile(P,N);
                fileID = fopen(filename);
                C = textscan(fileID,'%f %f');
                fclose(fileID);
                C{1};
                if all(ismember(ecg_peak1,C{1})==1)
                    %disp('test');
                    continue;
                end
            end
            
            %   old feature
            point_inflection = normalized_ppg(inf_post);
            point_max = normalized_ppg(ppg_peak1);
            b = point_max - point_inflection;
            a = point_max;
            
            %% GENERATE FEATURES
            ai = b/a;
            f_ai_values = [f_ai_values; ai];
            
            the_first_point = [the_first_point;ecg_peak1];
            the_second_point = [the_second_point; ecg_peak2];

            hr = 60*sampling_rate/(ecg_peak2-ecg_peak1);     
            f_hr_values = [f_hr_values; hr];

            ptt = (slope - ecg_peak1)/sampling_rate;
            f_ptt_values = [f_ptt_values; ptt];

            up_time = (ppg_peak1 - PPG_min_positions)/sampling_rate;
            f_uptime_values = [f_uptime_values; up_time];

            sist_time = (dict_notch - PPG_min_positions)/sampling_rate;
            f_syst_time_values = [f_syst_time_values; sist_time];

            distance_dv = trapz(part_dv);                
            f_dv_values = [f_dv_values; distance_dv];

            distance_sv = trapz(part_sv);               
            f_sv_values = [f_sv_values; distance_sv];
            
            %% GENERATE GT
            gt_len = 400;
            abp_len = length(part_abp);
            
            if gt_len >= abp_len
               gt_abp = dftinterp([1,abp_len], part_abp', 1, gt_len);
           else
               gt_abp = resample(part_abp', gt_len, abp_len, 1, 0);
            end
           
            gt_abp_values = [gt_abp_values; gt_abp];
        end
    end
    
    if isequal(size(the_first_point), size(the_second_point), size(f_ptt_values), size(f_hr_values), ...
            size(f_ai_values), size(f_syst_time_values), size(f_uptime_values), size(f_sv_values), ...
            size(f_dv_values), size(gt_abp_values))
        for i = 1: length(f_ptt_values)-1
            f_dptt1_values = [f_dptt1_values abs(f_ptt_values(i)-f_ptt_values(i+1))];
            f_duptime1_values = [f_duptime1_values abs(f_uptime_values(i)-f_uptime_values(i+1))];
            f_dsyst_time1_values = [f_dsyst_time1_values abs(f_syst_time_values(i)-f_syst_time_values(i+1))];
        end

        for i = 1: length(f_dptt1_values)-1
            f_dptt2_values = [f_dptt2_values abs(f_dptt1_values(i)-f_dptt1_values(i+1))];
            f_duptime2_values = [f_duptime2_values abs(f_duptime1_values(i)-f_duptime1_values(i+1))];
            f_dsyst_time2_values = [f_dsyst_time2_values abs(f_dsyst_time1_values(i)-f_dsyst_time1_values(i+1))];
        end
    else
        continue
    end
    
   %% SAVE THE FEATURES INTO 1 FILE FOR EACH SUBJECT
        parameter_features = [the_first_point the_second_point f_ptt_values f_hr_values f_ai_values f_syst_time_values f_uptime_values f_sv_values ...
            f_dv_values f_dptt1_values' f_dptt2_values' f_duptime1_values' f_duptime2_values' f_dsyst_time1_values' f_dsyst_time2_values' gt_abp_values];
        file_name = sprintf('./Parameter_features_with_13_units_Part_4/Subject_new_%i.mat',id);
        save(file_name,'parameter_features','-v4') ;
end